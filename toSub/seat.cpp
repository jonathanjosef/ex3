#include "seat.h"

// === Basic Functions === //

unsigned int Seat::getLine() const {
	return line;
}
unsigned int Seat::getChair() const {
	return chair;
}

char RegularSeat::getZone() const {
	return zone;
}

// === Exceptions === //

const char* NoPrice::what() const throw() {
	return "Not For Sale !";
}

// === Price Function === //
int Seat::price() const{
	if (basicPrice == GreenRoomSeatError)
		throw NoPrice();
	
	return basicPrice;
}

// === Location Functions === //

// --- Abstract --- //
string Seat::location() const{
	return string("line: ") + std::to_string(getLine()) +
		string(", chair: ") + std::to_string(getChair());
}

string RegularSeat::location() const {
	return string("area: ") + getZone() + string(", ") + Seat::location();
}

// --- Derived --- //

string FrontRegularSeat::location() const {
	return string("Front-> ") + RegularSeat::location();
}
string MiddleRegularSeat::location() const {
	return string("Middle-> ") + RegularSeat::location();
}
string RearRegularSeat::location() const {
	return string("Rear-> ") + RegularSeat::location();
}
string GoldenCircleSeat::location() const {
	return string("Golden Circle-> ") + Seat::location();
}
string DisablePodiumSeat::location() const {
	return string("Disable Podium-> ") + Seat::location();
}

string GreenRoomSeat::location() const{
	return string("Green Room-> ") + Seat::location();
}