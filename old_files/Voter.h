#pragma once
#include <string>

#define JUDGE_MAX_VOTES 10

using namespace std;

typedef std::string State;

enum VoterType { All, Regular, Judge};



class Voter
{
	State const voterState;
	VoterType const type;
	int votingTimes;
	
	Voter(const Voter& voter) = default;
	Voter& operator=(const Voter& voter) = default;

	public:
		Voter(State state, VoterType voterType = Regular) :
			voterState(state), type(voterType), votingTimes(0) {}

		~Voter() = default;
		State state() const;
		VoterType voterType() const;
		int timesOfVotes() const;
		Voter& operator++();
		Voter operator++(int);

};

ostream& operator<<(ostream& os, const Voter& voter);

enum stateRanks {First, Second, Third, Fourth, Fifth, Sixth, Seventh, Eighth,
	Ninth, Tenth};

struct Vote
{
	Voter& voter;

	//states
	//Will be initilize on the c'tor. "" is empty cell, move forword to the 
	//next cell. Do not end the run for a case of recieving "" as an input. 
	State states[JUDGE_MAX_VOTES];

	// My eyes burned too... 
	Vote(Voter& voter, State state1, State state2 = "", State state3 =""
		, State state4 = "", State state5 = "", State state6 = ""
		, State state7 = "", State state8 = "", State state9 = "",
		State state10 = "") :
		voter(voter) {
			states[First] = state1;
			states[Second] = state2;
			states[Third] = state3;
			states[Fourth] = state4;
			states[Fifth] = state5;
			states[Sixth] = state6;
			states[Seventh] = state7;
			states[Eighth] = state8;
			states[Ninth] = state9;
			states[Tenth] = state10;
	}
};