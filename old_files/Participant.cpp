#include "Participant.h"

State Participant::state() const {
    return _state;
}

string Participant::song() const {
    return _song;
}

int Participant::timeLength() const {
    return _timeLength;
}

string Participant::singer() const {
    return _singer;
}

bool Participant::isRegistered() const {
    return _isRegistered;
}

void Participant::update(string song, int timeLength, string singer) {
    if (_isRegistered) return;
    if (song != "") _song = song;
    if (timeLength > 0) _timeLength = timeLength;
    if (singer != "") _singer = singer;
}

void Participant::updateRegistered(bool isRegistered) {
    _isRegistered = isRegistered;
}

std::ostream& operator<<(std::ostream& os, const Participant& p) {
    return os << '[' << p.state() << '/'
        << p.song() << '/'
        << p.timeLength() << '/'
        << p.singer() << ']';
}
