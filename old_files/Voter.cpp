#include <string>
#include "Voter.h"

static string printType(const Voter& v){
	string types [] = { "All", "Regular", "Judge" };
	return types[v.voterType()];
}

Voter& Voter::operator++(){
	(this->votingTimes)++;
	return *this;
}
Voter Voter::operator++(int) {
	Voter temp(this->state(),this->voterType());
	(this->votingTimes)++;
	return temp;
}

State Voter::state() const{
	return voterState;
}

VoterType Voter::voterType() const {
	return type;
}

ostream& operator<<(ostream& os, const Voter& voter) {
	os << '<' << voter.state() << '/' << printType(voter) << '>';

	return os;
}


int Voter::timesOfVotes()const {
	return this->votingTimes; 
}
