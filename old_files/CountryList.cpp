#include "CountryList.h"
#include <assert.h>
using Iterator = CountryList::Iterator;

static const int SCORES_COUNT = 10;
static const int* SCORES =
new int[SCORES_COUNT] { 12, 10, 8, 7, 6, 5, 4, 3, 2, 1 };

void CountryList::add(Participant& p) {
    if (length() >= maxLength) return;

    if (_first == NULL || p.state() < _first->participant.state()) {
        _first = new CountryNode(p.state(), p, _first);
        _length++;
		p.updateRegistered(true);
        return;
    }
	if (_first != NULL && p.state() == _first->state)
		return;
	
	
    CountryNode* previous = NULL;
    // can't use range-based for! need to modify list
    for (CountryNode* node = _first; node != NULL; node = node->next) {
		//a check if the country is already exist.
		//TODO: maybe free previous before return
		if (node->next != NULL && p.state() == node->next->state) return;
        if (node->next == NULL || p.state() < node->next->state) {
            previous = node;
            break;
        }
    }
    if (previous != NULL) {
        previous->next = new CountryNode(p.state(), p, previous->next);
		p.updateRegistered(true);
        _length++;
    }
}

void CountryList::remove(Participant& p) {
    if (_first != NULL && &_first->participant == &p) {
		p.updateRegistered(false);
        CountryNode* temp = _first;
        _first = _first->next;
        delete temp;
        _length--;
        return;
    }
    // can't use range-based for! need to modify list
    for (CountryNode* node = _first; node != NULL; node = node->next) {
        if (node->next != NULL && &node->next->participant == &p) {
            CountryNode* temp = node->next;
            node->next = node->next->next;
            delete temp;
            _length--;
            break;
        }
    }
}

void CountryList::add(const Vote& vote) {
    if (!participate(vote.voter.state())) return;
    switch (vote.voter.voterType()) {
    case Regular:
        if (vote.voter.state() == vote.states[0]) return;
		for (CountryNode* node = _first; node != NULL; node = node->next) {
            if (node -> state == vote.states[0]) {
                node ->regularVotes++;
                vote.voter++;
                break;
            }
        }
        break;
    case Judge:
        for (int i = 0; i < SCORES_COUNT; i++) {
            if (vote.voter.state() == vote.states[i]) continue;
			for (CountryNode* node = _first; node != NULL; node = node->next) {
                if (node -> state == vote.states[i]) {
                    node -> judgeVotes += SCORES[i];
                    vote.voter++;
                }
            }
        }
        break;
    }
}

bool CountryList::participate(State state) const {
    for (CountryNode node : *this) {
        if (node.state == state) return true;
    }
    return false;
}

int CountryList::length() const {
    return _length;
}

Iterator CountryList::begin() const {
    return Iterator(_first);
}

Iterator CountryList::end() const {
    return Iterator(NULL);
}

void CountryList::setComparisonMode(VoterType mode) {
    _first->setComparisonMode(mode);
}

void CountryNode::setComparisonMode(VoterType mode) {
    comparisonMode = mode;
    if (next != NULL) next->setComparisonMode(mode);
}

bool CountryNode::operator<(const CountryNode& other) const {
    int sum, otherSum;
    switch (comparisonMode) {
    case All:
        sum = regularVotes + judgeVotes;
        otherSum = other.regularVotes + other.judgeVotes;
        return sum < otherSum;
    case Regular: return regularVotes < other.regularVotes;
    case Judge: return judgeVotes < other.judgeVotes;
    }
    assert(false); // NOT SUPPOSED TO HAPPEN!!!
    return false;
}

Iterator Iterator::operator++() {
    data = data->next;
    return *this;
}

bool Iterator::operator!=(const Iterator& other) const {
    return data != other.data;
}

const CountryNode& Iterator::operator*() const {
    return *data;
}