#pragma once

#include "CountryList.h"
#include "Voter.h"
#include "Participant.h"
#define DEFAULT_MAX_TIME	180
#define DEFAULT_MAX_PARTICIPANT 26
#define DEFAULT_MAX_VOTES	5

enum Phase { Registration, Contest, Voting };

class MainControl
{
public:
    struct Iterator {
        Iterator() { }
        Iterator(CountryNode* data) : data(data) { }
        bool operator<(const Iterator&) const;
        bool operator==(const Iterator&) const;
        Iterator operator++();
        const Participant& operator*() const;

    private:
        CountryNode* data;
    };

private:
    int const maxSongLength;
    int const maxParticipantsAllowed;
    int const maxVotes;
    CountryList countryList;
    Phase phase;

public:
    MainControl(int maxSongLength = DEFAULT_MAX_TIME, 
		int maxParticipantsAllowed = DEFAULT_MAX_PARTICIPANT, 
		int maxVotes = DEFAULT_MAX_VOTES) 
		:	maxSongLength(maxSongLength),
        maxParticipantsAllowed(maxParticipantsAllowed),
        maxVotes(maxVotes), countryList(maxParticipantsAllowed),
        phase(Registration) {
    }

    ~MainControl() = default;


    /*  >>>>> legalParticipant <<<<<
    Check if a participant can be is legal.
    Note: Do not check if can be registered! for this use checkParticipant.

    return false if:
    1. The participant strings is not valid (name, song, state)
    2. The song lenght isn't positive.
    3. The song longer then the max length allowed.

    else, return true.
    */
    bool legalParticipant(Participant&) const;


    /* >>>>> setPhase <<<<<
    Change this phase
    It's possible to move ONLY from Registration to Contest and from Contest to
    Voting. in all other cases nothing is done and the operation is ignored.
    */
    void setPhase(Phase);

    /* >>>>> participate <<<<<
        receive state, return true if the state have a registered participant,
        and false if it doesn't.
    */
    bool participate(State) const;

    /* >>>>> operator += Participant <<<<<
        Check if participant can be registered (look on checkParticipant() for
        more info).
        in a case of valid participant, add him to country list and ++ it's
        size.
    */
    MainControl& operator+=(Participant&);


    /* >>>>> operator -= Paticipate <<<<<
        check if mainControl on registration phase and if the participant is
        registered.

        remove it from the contest if it is, does nothing if it isn't.
    */
    MainControl& operator-=(Participant&);

    /* >>>>> operator += Vote <<<<<
        Check mainControl on Voting phase and that the voter can vote in terms
        of max votes.
    */
    MainControl& operator+=(const Vote&);
	
    // returns iterator which points to
    // first CountryNode in internal CountryList
    Iterator begin() const;

    // returns iterator which points to one after the
    // last CountryNode in the internal CountryList
	Iterator end() const;

	//Print the mainControl. using COUNTRY_FOREACH. Need to have access to phase
	friend std::ostream& operator<<(std::ostream&, const MainControl&);
};



std::ostream& operator<<(std::ostream&, const MainControl::Iterator&);