#pragma once

#include "Participant.h"
#include "Voter.h"

//TODO contents of this (most) file(s) go in eurovision.cpp once we merge! this is NOT part of the public API!

struct CountryNode {
    const State state;
    const Participant& participant;
    int regularVotes = 0;
    int judgeVotes = 0;
    CountryNode* next;

    CountryNode(State state, const Participant& participant,
                CountryNode* next = NULL) :
        state(state), participant(participant), next(next)
    { }

    // sets comparison mode for this and next CountryNode
    void setComparisonMode(VoterType mode);
    bool operator<(const CountryNode& other) const;

private:
    VoterType comparisonMode;
};

struct CountryList {

    struct Iterator {
        Iterator(CountryNode* data) : data(data) { }
        Iterator operator++();
        bool operator!=(const Iterator& other) const;
        const CountryNode& operator*() const;
    private:
        CountryNode* data;
    };

    // max length of list, given in constructor.
    const int maxLength;

    CountryList(int maxLength) : maxLength(maxLength) { }
    ~CountryList() {
        CountryNode* iter = _first;
        while (iter != NULL) {
            CountryNode* temp = iter;
            iter = iter->next;
            delete temp;
        }
    }

    // adds given participant to inner list, keeping it sorted by state name.
    // assumes we're in registration phase, and that the participant would've
    // returned true from MainControl::legalParticipant.
    // does nothing if too many participants or if country already registered.
    void add(Participant&);

    // removes participant from inner list.
    // assumes we're in registration phase.
    // does nothing if participant not found.
    void remove(Participant&);

    // adds vote to inner list, and increments voter's vote count.
    // assumes we're in voting phase, that Vote has no duplicate states,
    // and that the Voter can still vote. (hasn't exceeded maximum)
    // does nothing for each state in the vote that
    // doesn't exist, or is the voter's own state.
    void add(const Vote&);

    // returns if state exists in inner list.
    // (yes, exactly like MainControl::participate)
    bool participate(State) const;

    // returns amount of states in inner list
    int length() const;

    // sets comparison mode for internal CountryNodes
    void setComparisonMode(VoterType mode);

    // Iterator implementation : allows for(CountryNode x : countryList)
    Iterator begin() const;
    Iterator end() const;

private:
    int _length = 0;
    CountryNode* _first = NULL;
};
