#ifndef SEAT_H_
#define SEAT_H_

#include <iostream>
#include <string>

using std::string;
using std::exception;

enum Prices {GreenRoomSeatError = -1, RearRegularSeatAdd = 0, 
			MainHallSeatAdd = 100, MiddleRegularSeatAdd = 250,
			SpecialSeatAdd =300, FrontRegularSeatAdd = 500, 
			DisablePodiumSeatConst = 200 - MainHallSeatAdd - SpecialSeatAdd, 
			GoldenCircleSeatAdd = 1000};

// ---------------------------------------------
class NoPrice : public exception
{
public:
	const char* what() const throw() override;
};

// ---------------------------------------------
class Seat
{
private:
	unsigned int const line;
	unsigned int const chair;
	int const basicPrice;

protected:
	//Every derived class should override this.
	virtual bool isAbstract() = 0;

public:
	Seat(int line, int chair, int basicPrice) : line(line), chair(chair),
		basicPrice(basicPrice) {};
	virtual ~Seat() = default;

	unsigned int getLine() const;
	unsigned int getChair() const;
	int price()const;
	virtual string location()const;
};

// ---------------------------------------------
class GreenRoomSeat : public Seat
{
private:
	bool isAbstract() override { return false; }

public:
	GreenRoomSeat(int line, int chair) : 
		Seat(line, chair, GreenRoomSeatError) {}
	~GreenRoomSeat() = default;

	string location() const;
};

// ---------------------------------------------
class MainHallSeat : public Seat
{
public:
	MainHallSeat(int line, int chair, int price) :
		Seat(line, chair, MainHallSeatAdd + price) {}
	~MainHallSeat() = default;
};

// ---------------------------------------------
class SpecialSeat : public MainHallSeat
{
public:
	SpecialSeat(int line, int chair, int price) : 
		MainHallSeat(line, chair, SpecialSeatAdd + price) {}
	~SpecialSeat() = default;
};

// ---------------------------------------------
class GoldenCircleSeat : public SpecialSeat
{
private:
	bool isAbstract() override { return false; }
	
public:
	string location()const override;
	GoldenCircleSeat(int line, int chair, int basicPrice) : 
		SpecialSeat(line, chair, GoldenCircleSeatAdd + basicPrice) {}
	~GoldenCircleSeat() = default;
};

// ---------------------------------------------
class DisablePodiumSeat : public SpecialSeat
{
private:
	bool isAbstract() override { return false; }

public:
	string location()const override;
	DisablePodiumSeat(int line, int chair, int price = DisablePodiumSeatConst):
		SpecialSeat(line, chair, DisablePodiumSeatConst) {}
	~DisablePodiumSeat() = default;
};

// ---------------------------------------------
class RegularSeat : public MainHallSeat
{
private:
	char zone;

public:
	char getZone() const;
	virtual string location() const;
	RegularSeat(char zone, int line, int chair, int price) :
		 MainHallSeat(line, chair, price), zone(zone) {}
	~RegularSeat() = default;
};

// ---------------------------------------------
class FrontRegularSeat : public RegularSeat
{
private:
	bool isAbstract() override { return false; }

public:
	FrontRegularSeat(char zone, int line, int chair, int price) :
		RegularSeat(zone, line, chair, FrontRegularSeatAdd + price) {}
	~FrontRegularSeat() = default;

	string location() const;
};

// ---------------------------------------------
class MiddleRegularSeat: public RegularSeat
{
private:
	bool isAbstract() override { return false; }

public:
	MiddleRegularSeat(char zone, int line, int chair, int price) :
		RegularSeat(zone, line, chair, MiddleRegularSeatAdd + price) {}
	~MiddleRegularSeat() = default;

	string location() const;
};

// ---------------------------------------------
class RearRegularSeat : public RegularSeat
{
private:
	bool isAbstract() override { return false; }

public:
	RearRegularSeat(char zone, int line, int chair, int price) :
		RegularSeat(zone, line, chair, RearRegularSeatAdd + price) {}
	~RearRegularSeat() = default;

	string location() const;
};
// ---------------------------------------------


#endif