#pragma once

#include "Voter.h"

#include <string>
using std::string;

class Participant
{
    const State _state;
    string _song;
    int _timeLength;
    string _singer;
    bool _isRegistered;

    // hide the copy constructor and the operator=. The empty constructor is
    // automatically unavailable because of the defined constructor below
    Participant(const Participant&) = default;
    Participant& operator=(const Participant&) = default;

public:
    Participant(State state, string song, int timeLength, string singer)
        : _state(state), _song(song), _timeLength(timeLength), _singer(singer),
		_isRegistered(false)
    { }

    State state() const;
    string song() const;
    int timeLength() const;
    string singer() const;
    bool isRegistered() const;
    void update(string, int, string);
    void updateRegistered(bool);
};

std::ostream& operator<<(std::ostream&, const Participant&);
