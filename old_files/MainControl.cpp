#include "MainControl.h"
using Iterator = MainControl::Iterator;

template<class Iter>
Iter get(Iter start, Iter end, int i) {
    
    // assumptions about Iterator: (reverse-engineered from CountryList::Iterator)
    //     unary ++  (move forward)
    //     binary != (compare iterator position)
    //     unary *   (get content)
    // assumptions about Iterator.data:
    //     binary <  (compare)
    //TODO implement!!!
}


/*
CountryList::Iterator get(CountryList::Iterator start, CountryList::Iterator end, int i) {

}*/

bool MainControl::legalParticipant(Participant& participant) const {
	if ((participant.state().length() <= 0)		||
		(participant.song().length() <= 0)		||
		(participant.singer().length() <= 0)	||
		(participant.timeLength() <= 0)			||
		(participant.timeLength() > maxSongLength)) {
		return false;
	}

	return true;
}

void MainControl::setPhase(Phase newPhase) {
	if ((phase == Registration && newPhase == Contest) ||
		(phase == Contest && newPhase == Voting))
		phase = newPhase;
}

bool MainControl::participate(State state) const {
	return countryList.participate(state);
}

MainControl& MainControl::operator+=(Participant& participant) {
	if ((phase == Registration) &&
		(legalParticipant(participant))) {
		countryList.add(participant);
	}
		
	return *this;
}

MainControl& MainControl::operator-=(Participant& participant) {
	if(phase == Registration)
		countryList.remove(participant);

	return *this;
}

MainControl& MainControl::operator+=(const Vote& vote) {
	if ((phase == Voting)								&&
		((vote.voter.voterType() == Regular &&
			vote.voter.timesOfVotes() < maxVotes)		||
		(vote.voter.voterType() == Judge && vote.voter.timesOfVotes() == 0))) {
		countryList.add(vote);
	}

	return *this;
}


bool Iterator::operator<(const Iterator& other) const {
    return data != NULL && data != other.data;
}

bool Iterator::operator==(const Iterator& other) const {
    return data == other.data;
}

Iterator Iterator::operator++() {
    data = data->next;
    return *this;
}

const Participant& Iterator::operator*() const {
    return data->participant;
}




std::ostream& operator<<(std::ostream& os, const MainControl& mainControl) {
	string phaseString[] = { "Registration", "Contest", "Voting" };
	os << '{' << endl;
	os << phaseString[mainControl.phase] << endl;

	CountryList::Iterator i(mainControl.countryList.begin());

	if (mainControl.phase == Registration || mainControl.phase == Contest)
	{
		while (i != mainControl.countryList.end())
		{
			os << (*i).participant << endl;
			++i;
		}
	} else {
		while (i != mainControl.countryList.end())
		{
			os << (*i).participant.state() << " : Regular(";
			os << (*i).regularVotes << ") Judge(";
			os << (*i).judgeVotes << ')' << endl;
			++i;
		}
	}

	os << '}' << endl;
    return os;
}
