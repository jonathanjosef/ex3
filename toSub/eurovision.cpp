#include "eurovision.h"
#include <assert.h>

// === Participant ===

State Participant::state() const {
    return stateName;
}

string Participant::song() const {
    return songName;
}

int Participant::timeLength() const {
    return songLength;
}

string Participant::singer() const {
    return singerName;
}

bool Participant::isRegistered() const {
    return registered;
}

void Participant::update(string song, int timeLength, string singer) {
    if (registered) return;
    if (song != "") songName = song;
    if (timeLength > 0) songLength = timeLength;
    if (singer != "") singerName = singer;
}

void Participant::updateRegistered(bool isRegistered) {
    registered = isRegistered;
}

ostream& operator<<(ostream& os, const Participant& p) {
    return os << '[' << p.state() << '/'
        << p.song() << '/'
        << p.timeLength() << '/'
        << p.singer() << ']';
}

// --- Participant END ---

// === Voter ===

static string printType(const Voter& v) {
    switch (v.voterType())
    {
    case Regular: return "Regular";
    case Judge: return "Judge";
    default: // not supposed to happen!
        assert(false);
        return "";
    }
}

Voter& Voter::operator++() {
    votingTimes++;
    return *this;
}

State Voter::state() const {
    return voterState;
}

VoterType Voter::voterType() const {
    return type;
}

ostream& operator<<(ostream& os, const Voter& voter) {
    return os << '<' << voter.state() << '/' << printType(voter) << '>';
}

int Voter::timesOfVotes()const {
    return votingTimes;
}

// --- Voter END ---

// === StateList ===

static const int SCORES_COUNT = 10;
static const int SCORES[SCORES_COUNT] = { 12, 10, 8, 7, 6, 5, 4, 3, 2, 1 };

void MainControl::StateList::add(Participant& p) {
    if (length >= maxLength) return;
    if (first != NULL && p.state() == first->participant.state()) return;

    // separate check for first
    if (first == NULL || p.state() < first->participant.state()) {
        first = new StateNode(p.state(), p, first);
        p.updateRegistered(true);
        length++;
        return;
    }

    StateNode* previous = NULL;
    // can't use range-based for! need to modify list
    for (StateNode* node = first; node != NULL; node = node->next) {
        if (node->next == NULL || p.state() <= node->next->state) {
            previous = node;
            break;
        }
    }
    if (previous->next == NULL || p.state() < previous->next->state) {
        previous->next = new StateNode(p.state(), p, previous->next);
        p.updateRegistered(true);
        length++;
    }
}

void MainControl::StateList::remove(Participant& p) {
    // separate check for first
    if (first != NULL && &first->participant == &p) {
        p.updateRegistered(false);
        StateNode* temp = first;
        first = first->next;
        delete temp;
        length--;
        return;
    }
    // can't use range-based for! need to modify list
    for (StateNode* node = first; node != NULL; node = node->next) {
        if (node->next != NULL && &node->next->participant == &p) {
            p.updateRegistered(false);
            StateNode* temp = node->next;
            node->next = node->next->next;
            delete temp;
            length--;
            break;
        }
    }
}

void MainControl::StateList::add(const Vote& vote) {
    if (!participate(vote.voter.state())) return;
    int successfulVotes = 0;
    switch (vote.voter.voterType()) {
    case Regular:
        if (vote.voter.state() == vote.states[0]) return;
        for (StateNode& node : *this) {
            if (node.state == vote.states[0]) {
                node.regularVotes++;
                ++vote.voter;
                break;
            }
        }
        break;
    case Judge:
        for (int i = 0; i < SCORES_COUNT; i++) {
            if (vote.voter.state() == vote.states[i]) continue;
            for (StateNode& node : *this) {
                if (node.state == vote.states[i]) {
                    node.judgeVotes += SCORES[i];
                    successfulVotes++;
                }
            }
        }
        if (successfulVotes > 0) ++vote.voter;
        break;
    case All:
    default: // not supposed to happen!
        assert(false);
        break;
    }
}

bool MainControl::StateList::participate(State state) const {
    for (StateNode node : *this) {
        if (node.state == state) return true;
    }
    return false;
}

MainControl::StateList::Iterator MainControl::StateList::begin() const {
    return Iterator(first);
}

MainControl::StateList::Iterator MainControl::StateList::end() const {
    return Iterator(NULL);
}

void MainControl::StateList::setComparisonMode(VoterType mode) {
    first->setComparisonMode(mode);
}

void MainControl::StateNode::setComparisonMode(VoterType mode) {
    comparisonMode = mode;
    if (next != NULL) next->setComparisonMode(mode);
}

bool MainControl::StateNode::operator>(const StateNode& other) const {
    int sumDiff;
    switch (comparisonMode) {
    case All:
        sumDiff = (regularVotes + judgeVotes)
            - (other.regularVotes + other.judgeVotes);
        break;
    case Regular:
        sumDiff = regularVotes - other.regularVotes;
        break;
    case Judge:
        sumDiff = judgeVotes - other.judgeVotes;
        break;
    default: // not supposed to happen!
        assert(false);
        return false;
    }
    // guaranteed tiebreaker because state names are unique
    if (sumDiff == 0) return state > other.state;
    return sumDiff > 0;
}

MainControl::StateList::Iterator
MainControl::StateList::Iterator::operator++() {
    data = data->next;
    return *this;
}

bool MainControl::StateList::Iterator::operator!=
(const Iterator& other) const {
    return data != other.data;
}

MainControl::StateNode&
MainControl::StateList::Iterator::operator*() const {
    return *data;
}

// --- StateList END ---

// === MainControl ===

// used in template get. forms a generic linked list.
template<class Data>
struct GenericNode {
    Data data;
    GenericNode* next;

    GenericNode(Data data, GenericNode* next = NULL)
        : data(data), next(next) { }
};

// finds the i-th item in the given linked list, starting at first,
// and deletes the list.
// if the list is shorter than i, returns NULL.
// assumptions about Iterator are identical to template get
template<class Iterator>
static Iterator* findAndDelete(GenericNode<Iterator>* first, int i) {
    for (int j = 0; j < i; j++)
    {
        if (first == NULL) break;
        GenericNode<Iterator>* temp = first;
        first = first->next;
        delete temp;
    }
    if (first == NULL) return NULL;
    GenericNode<Iterator>* nodeAfter = first->next;
    while (nodeAfter != NULL) {
        GenericNode<Iterator>* temp = nodeAfter;
        nodeAfter = nodeAfter->next;
        delete temp;
    }
    Iterator* result = new Iterator(first->data);
    delete first;
    return result;
}

// Assumptions about class Iterator:
//     copy constructor
//     unary ++  (move forward)
//     binary != (compare iterator position)
//     unary *   (get content)
// assumptions about Iterator content:
//     binary >  (compare)
template<class Iterator>
Iterator get(Iterator start, Iterator end, int i) {
    GenericNode<Iterator>* first = new GenericNode<Iterator>(start);
    ++start;
    while (start != end) {
        // create a sorted linked list, in decreasing
        // order according to operator> of the content
        if (*start > *first->data) {
            first = new GenericNode<Iterator>(start, first);
        }
        else {
            GenericNode<Iterator>* node = first;
            while (node->next != NULL && *node->next->data > *start) {
                node = node->next;
            }
            node->next = new GenericNode<Iterator>(start, node->next);
        }
        ++start;
    }
    Iterator* result = findAndDelete(first, i);
    if (result == NULL) return end;
    Iterator inner = *result;
    delete result;
    return inner;
}

bool MainControl::legalParticipant(Participant& participant) const {
    if ((participant.state().length() <= 0) ||
        (participant.song().length() <= 0) ||
        (participant.singer().length() <= 0) ||
        (participant.timeLength() <= 0) ||
        (participant.timeLength() > maxSongLength)) {
        return false;
    }

    return true;
}

void MainControl::setPhase(Phase newPhase) {
    if ((phase == Registration && newPhase == Contest) ||
        (phase == Contest && newPhase == Voting))
        phase = newPhase;
}

bool MainControl::participate(State state) const {
    return stateList.participate(state);
}

MainControl& MainControl::operator+=(Participant& participant) {
    if ((phase == Registration) &&
        (legalParticipant(participant))) {
        stateList.add(participant);
    }
    return *this;
}

MainControl& MainControl::operator-=(Participant& participant) {
    if (phase == Registration) stateList.remove(participant);
    return *this;
}

MainControl& MainControl::operator+=(const Vote& vote) {
    if ((phase == Voting) &&
        ((vote.voter.voterType() == Regular &&
            vote.voter.timesOfVotes() < maxVotes) ||
            (vote.voter.voterType() == Judge &&
                vote.voter.timesOfVotes() == 0))) {
        stateList.add(vote);
    }
    return *this;
}

bool MainControl::Iterator::operator<(const Iterator& other) const {
    return data != NULL && data != other.data;
}

bool MainControl::Iterator::operator==(const Iterator& other) const {
    return data == other.data;
}

MainControl::Iterator MainControl::Iterator::operator++() {
    data = data->next;
    return *this;
}

const Participant& MainControl::Iterator::operator*() const {
    return data->participant;
}

MainControl::Iterator MainControl::begin() const {
    return MainControl::Iterator(&(*stateList.begin()));
}

MainControl::Iterator MainControl::end() const {
    return MainControl::Iterator(&(*stateList.end()));
}

const State MainControl::operator()(int position, VoterType type) {
    stateList.setComparisonMode(type);
    StateList::Iterator result =
        get(stateList.begin(), stateList.end(), position - 1);
    return (result != stateList.end()) ? (*result).state : "";
}

ostream& operator<<(ostream& os, const MainControl& mainControl) {
    string phaseString[] = { "Registration", "Contest", "Voting" };
    os << '{' << endl;
    os << phaseString[mainControl.phase] << endl;

    for (MainControl::StateNode state : mainControl.stateList) {
        switch (mainControl.phase) {
        case Registration:
            os << state.participant << endl;
            break;
        case Voting:
            os << state.participant.state() << " : Regular("
                << state.regularVotes << ") Judge("
                << state.judgeVotes << ')' << endl;
            break;
        default: break; // do nothing. intentional.
        }
    }

    os << '}' << endl;
    return os;
}

// --- MainControl END ---
