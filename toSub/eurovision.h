#ifndef EUROVISION_H_
#define EUROVISION_H_

#include <iostream>
#include <string>

const int JUDGE_MAX_VOTES = 10;
const int DEFAULT_MAX_TIME = 180;
const int DEFAULT_MAX_PARTICIPANT = 26;
const int DEFAULT_MAX_VOTES = 5;

using std::string;
using std::ostream;
using std::endl;
typedef string State;

// it's allowed to define here any using statements, according to needs.
// do NOT define here : using namespace std;

//---------------------------------------------------

enum VoterType { All, Regular, Judge };
enum Phase { Registration, Contest, Voting };

//---------------------------------------------------

class Participant
{
    const State stateName;
    string songName;
    int songLength;
    string singerName;
    bool registered = false;

    // Hide the copy constructor and the operator=. The empty constructor
    // is automatically unavailable because of the defined constructor below.
    Participant(const Participant&) = default;
    Participant& operator=(const Participant&) = default;

public:

    Participant(State state, string song, int timeLength, string singer)
        : stateName(state), songName(song),
        songLength(timeLength), singerName(singer)
    { }

    State state() const;
    string song() const;
    int timeLength() const;
    string singer() const;
    bool isRegistered() const;
    void update(string, int, string);
    void updateRegistered(bool);
};

ostream& operator<<(ostream&, const Participant&);

//---------------------------------------------------

class Voter
{
    const State voterState;
    const VoterType type;
    int votingTimes = 0;

    // Same as Participant - hide!
    Voter(const Voter& voter) = default;
    Voter& operator=(const Voter& voter) = default;

public:
    Voter(State state, VoterType voterType = Regular) :
        voterState(state), type(voterType) {}

    ~Voter() = default;
    State state() const;
    VoterType voterType() const;
    int timesOfVotes() const;
    Voter& operator++();
};

ostream& operator<<(ostream&, const Voter&);

// -----------------------------------------------------------

struct Vote
{
    Voter& voter;

    State states[JUDGE_MAX_VOTES];

    Vote(Voter& voter, State state1, State state2 = "", State state3 = ""
        , State state4 = "", State state5 = "", State state6 = ""
        , State state7 = "", State state8 = "", State state9 = "",
        State state10 = "") :
        voter(voter) {
        State states2[JUDGE_MAX_VOTES] = { state1, state2, state3,
            state4, state5, state6, state7, state8, state9, state10 };
        for (int i = 0; i < JUDGE_MAX_VOTES; i++) {
            states[i] = states2[i];
        }
    }
};
//----------------------------------------------------------

// Assumptions about class Iterator:
//     copy constructor
//     unary ++  (move forward)
//     binary != (compare iterator position)
//     unary *   (get content)
// assumptions about Iterator content:
//     binary >  (compare)
template<class Iterator>
Iterator get(Iterator start, Iterator end, int i);

class MainControl
{
    // -- Subclasses --
private:

    // A single node in StateList, that holds
    // all relevant info about a single state.
    struct StateNode {
        const State state;
        Participant& participant;
        int regularVotes = 0;
        int judgeVotes = 0;
        StateNode* next;

        StateNode(State state, Participant& participant,
            StateNode* next = NULL) :
            state(state), participant(participant), next(next)
        { }

        // sets comparison mode for this and next StateNode
        void setComparisonMode(VoterType mode);

        // affected by comparisonMode!
        bool operator>(const StateNode& other) const;

    private:
        VoterType comparisonMode;
    };

    // Holds all states in the system, and allows easy operations on them.
    struct StateList {

        struct Iterator {
            Iterator(StateNode* data) : data(data) { }
            Iterator operator++();
            bool operator!=(const Iterator& other) const;
            StateNode& operator*() const;
        private:
            StateNode* data;
        };

        // max length of list, given in constructor.
        const int maxLength;

        StateList(int maxLength) : maxLength(maxLength) { }
        ~StateList() {
            while (first != NULL) {
                StateNode* temp = first;
                first = first->next;
                temp->participant.updateRegistered(false);
                delete temp;
            }
        }

        // adds given participant to inner list,
        // keeping it sorted by state name.
        // assumes we're in registration phase, and that the participant
        // would've returned true from MainControl::legalParticipant.
        // does nothing if too many participants,
        // or if state already registered.
        void add(Participant&);

        // removes participant from inner list.
        // assumes we're in registration phase.
        // does nothing if participant not found.
        void remove(Participant&);

        // adds vote to inner list, and increments voter's vote count.
        // assumes we're in voting phase, that Vote has no duplicate states,
        // and that the Voter can still vote. (hasn't exceeded maximum)
        // does nothing for each state in the vote that
        // doesn't exist, or is the voter's own state.
        void add(const Vote&);

        // returns if state exists in inner list.
        bool participate(State) const;

        // sets comparison mode for internal StateNodes
        void setComparisonMode(VoterType mode);

        // Iterator implementation : allows for(StateNode x : stateList)
        Iterator begin() const;
        Iterator end() const;

    private:
        int length = 0;
        StateNode* first = NULL;
    };

public:
    struct Iterator {
        Iterator() { }
        Iterator(StateNode* data) : data(data) { }
        bool operator<(const Iterator&) const;
        bool operator==(const Iterator&) const;
        Iterator operator++();
        const Participant& operator*() const;

    private:
        StateNode* data;
    };

    // -- MainControl --
private:
    const int maxSongLength;
    const int maxParticipantsAllowed;
    const int maxVotes;
    StateList stateList;
    Phase phase = Registration;

public:
    MainControl(int maxSongLength = DEFAULT_MAX_TIME,
        int maxParticipantsAllowed = DEFAULT_MAX_PARTICIPANT,
        int maxVotes = DEFAULT_MAX_VOTES) : maxSongLength(maxSongLength),
        maxParticipantsAllowed(maxParticipantsAllowed), maxVotes(maxVotes),
        stateList(maxParticipantsAllowed) { }

    /*  >>>>> legalParticipant <<<<<
    Check if a participant can be is legal.
    Note: Do not check if can be registered! for this use checkParticipant.

    return false if:
    1. The participant strings is not valid (name, song, state)
    2. The song length isn't positive.
    3. The song longer then the max length allowed.

    else, return true.
    */
    bool legalParticipant(Participant&) const;

    /* >>>>> setPhase <<<<<
    Change this phase
    It's possible to move ONLY from Registration to Contest and from Contest
    to Voting. in all other cases nothing is done and the operation is ignored.
    */
    void setPhase(Phase);

    /* >>>>> participate <<<<<
        receive state, return true if the state have a
        registered participant, and false if it doesn't.
    */
    bool participate(State) const;

    /* >>>>> operator += Participant <<<<<
        Check if participant can be registered (see also checkParticipant())
        in a case of valid participant, add him to state list and ++ its size.
    */
    MainControl& operator+=(Participant&);

    /* >>>>> operator -= Paticipate <<<<<
        check if mainControl on registration, then remove given participant
        from the contest. Does nothing if it wasn't found anyway.
    */
    MainControl& operator-=(Participant&);

    /* >>>>> operator += Vote <<<<<
        Check mainControl on Voting phase and that
        the voter can vote in terms of max votes.
    */
    MainControl& operator+=(const Vote&);

    // returns state that scored "i"-th place,
    // counting by the given VoterType.
    const State operator()(int, VoterType);

    // returns iterator which points to
    // first StateNode in internal StateList
    Iterator begin() const;

    // returns iterator which points to one after the
    // last StateNode in the internal StateList
    Iterator end() const;

    // prints the MainControl.
    // needs to be friend for access to phase and stateList!
    friend ostream& operator<<(ostream&, const MainControl&);
};

// -----------------------------------------------------------

#endif
